<!--
Copyright (c) 2020 Paul Barker <pbarker@konsulko.com>
SPDX-License-Identifier: CC-BY-4.0
-->

# ChangeLog for mirrorshades

## 0.1.3

Minor release of mirrorshades.

* Add setup.py script to support legacy build environments.

## 0.1.2

Bugfix release of mirrorshades.

* Make dependency on `gitlab` python module optional.

## 0.1.1

(yanked)

## 0.1.0

Initial release of mirrorshades.

* Supports mirroring git repositories by repository URL.

* Supports mirroring git repositories from a GitLab server using project or
  group paths. For groups, all projects within the group are mirrored.

* Supports mirroring from remote and cloud storage locations using rclone.

* Supports invoking custom commands to mirror from arbitrary sources.
